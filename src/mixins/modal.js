
//  ____    ____   ___   ______        _       _____          ____    ____  _____  ____  ____  _____  ____  _____  
//  |_   \  /   _|.'   `.|_   _ `.     / \     |_   _|        |_   \  /   _||_   _||_  _||_  _||_   _||_   \|_   _| 
//    |   \/   | /  .-.  \ | | `. \   / _ \      | |            |   \/   |    | |    \ \  / /    | |    |   \ | |   
//    | |\  /| | | |   | | | |  | |  / ___ \     | |   _        | |\  /| |    | |     > `' <     | |    | |\ \| |   
//   _| |_\/_| |_\  `-'  /_| |_.' /_/ /   \ \_  _| |__/ |      _| |_\/_| |_  _| |_  _/ /'`\ \_  _| |_  _| |_\   |_  
//  |_____||_____|`.___.'|______.'|____| |____||________|     |_____||_____||_____||____||____||_____||_____|\____| 
//                                                                                                                 
 

export const modal = {
  data: function(){
    return {} 
  },
  props: {
    payload: {
      type: Object,
      required: false,
      default: ()=>{ return {} }
    },
    errors: {
      type: Object,
      required: false,
      default: ()=>{ return {} }      
    },
  },
  emits: ['close', 'updateValue'],
  computed:{
    item: { 
      get: function() {
        return this.payload.item 
      },
      set: function (newPayload){
        this.$store.commit("modal/setModalPayload", newPayload)
      }
    },
  },
  methods: {
    /**
     * Clone Modal
     */
    closeModal: function(){
      this.$store.dispatch("modal/closeDialog")
    },
  },
  mounted: function(){},
}
