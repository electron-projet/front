import { createApp } from 'vue'
// import * as Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Vue.config.productionTip = false

let app = createApp(App)
app.use(router)
app.use(store)

/* API services initialization */
import APIs from '@/services'
APIs.init(app)

/* import base components */
import Base from '@/base-front/main'
Base.initBaseComponents(app)

app.mount('#app')
