import { createWebHistory, createRouter } from "vue-router";

// Declare Routes to get differents chunks 
// Doc : https://router.vuejs.org/guide/advanced/lazy-loading.html#grouping-components-in-the-same-chunk
const Home = () => import(/* webpackChunkName: "home" */'@/views/HomePage')

// Vue.use(Router)

const routes = [
    {
      path: '/',
      name: 'home',
      component: Home,
    }, 
]

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
