const state = {
  endpoint: {},
  table: null
}

const mutations = {
  setEndpoint: function(state, endpoint) {
      state.endpoint = endpoint
  },
  setTable: function(state, value) {
    state.table = value
  }
}

const actions = {
  initStore: function({ commit }, endpoint) {
    commit('setEndpoint', endpoint)
  },
  search: function({ commit }) {
    return state.endpoint
      .search()
      .then((response) => {
        console.debug('TABLE: ✔ ok', response.data.result)
        commit('setTable', response.data.result)
        return true
      })
      .catch((error) => {
        console.debug('TABLE: ❌ failed', error)
        return false
      })
  },
}

export default{
  namespaced: true,
  state,
  mutations,
  actions,
}
