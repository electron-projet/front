const state = {
  modalShow: false,
  modalPayload: {},
  modalErrors: {},
  modalList: [],
}

const mutations = {
  setModalShow(state, value){
    state.modalShow = value
  },
  setModalPayload(state, value){
    state.modalPayload = value
  },
  setModalErrors(state, value){
    state.modalErrors = value
  },  
  /**
   * Pop the last iteration of array if modal is null
   * In the other case push value to array
   * @param {*} state 
   * @param {String} modal: add or null
   */
  setModalList(state, modal=null){
    // modal == null means we need to pop() in array
    if(modal == null){
      state.modalList.pop()
    }
    else if(modal.action === 'add' && modal.value){
      state.modalList.push(modal.value)
    } 
  }, 
}

const actions = {
  /**
   * Open Modal
   * Several modal can be called in a row
   * @param {Object} payload 
   * @returns 
   */
  openDialog({commit}, payload){
    return new Promise((resolve, reject) => {
      payload = {...payload, promise : {resolve:resolve, reject:reject}}
      commit("setModalPayload", payload)
      commit("setModalList", { action: 'add', value: payload.modalModel })
      commit("setModalShow", true)
      commit("setModalErrors", {})
    })
  },
  /**
   * Close the last modal called 
   */
  closeDialog({commit}){
    commit("setModalShow", false)
    commit("setModalList")
  },
  /**
   * method to manage errors
   * @param {Object} errors 
   */
  setModalErrors({commit}, errors){
    if (!errors) {
      commit("setModalShow", false)
    } else {
      commit("setModalErrors", errors)
    }
  },
}

export default{
  namespaced: true,
  state,
  mutations,
  actions,
}
