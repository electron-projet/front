import  { createStore } from 'vuex'
// import createPersistedState from 'vuex-persistedstate'

/* STORE MODULES */
import table from './modules/table'
import modal from './modules/modal'

const debug = process.env.NODE_ENV !== 'production'

export default createStore({
  modules: {
    table,
    modal,
  },
  strict: debug,
})
