class BaseModel {
  constructor (handler, url) {
    this.handler = handler
    this.baseURL = url
  }
  
  search = (query) => this.handler.get(this.baseURL, { params: query })
  getByID = (id) => this.handler.get(this.baseURL + id + '/')
  optionsByID = (id) => this.handler.options(this.baseURL + id + '/')
  create = (payload) => this.handler.post(this.baseURL, payload)
  update = (id, payload) => this.handler.patch(this.baseURL + id, payload)
  destroy = (id) => this.handler.delete(this.baseURL + id)
}

export default BaseModel

