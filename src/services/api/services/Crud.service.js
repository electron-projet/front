import defaultOptions from '../default/options.js'
import { defaultRequestInterceptor, defaultResponseInterceptor } from '../default/interceptors.js'
import DefaultMethods from '../default/methods'

/* --------------- Service static configuration ------------- */
// Axios requests settings
const config = {
  ...defaultOptions,
  // xsrfCookieName: 'csrftoken',
  // xsrfHeaderName: 'x-csrftoken',
  // withCredentials: true,
}

// API base URL
const baseURL = {
  protocol: process.env.VUE_APP_API_PROTOCOL,
  host: process.env.VUE_APP_API_HOST,
  port: process.env.VUE_APP_API_PORT,
  root: process.env.VUE_APP_API_ROOT,
}

// API not-model URLs ("special")
const routesURLs = {
  tables: '/tables/',
  lists: '/lists/',
  tasks: '/tasks/',
}
/* --------------- Interceptors customization ------------ */
const interceptors = {
  request: { ...defaultRequestInterceptor }, // We inject the default interceptors as needed
  response: { ...defaultResponseInterceptor }
}

/* ---------------- "Special" methods ---------- */
// eslint-disable-next-line no-unused-vars
const routesMethods = (handler, routeName, url) => {
  const baseMethods = new DefaultMethods(handler, url)
  const route = {
    tables: {},
    lists: {
      getListByTable: (id) => handler.get(url + "table/" + id)
    },
    tasks: {},
  }
  return { ...baseMethods, ...route[routeName] }
}

export default {
  config,
  baseURL,
  interceptors,
  routesURLs,
  routesMethods
}
