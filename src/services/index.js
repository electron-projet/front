// import { createApp } from 'vue'
import axios from 'axios'

/*
  This parses the "api" folder and
  considers every "*.service.js" file as an enabled service.
  ⚠ Please chose service name short and comprehensible and avoid ES keywords.
*/
const availableAPIs = require.context(
  '@/services/api/services',
  false,
  /\.service\.js$/
)

/*
  All found services are initialized
*/
const init = (app) => {
  availableAPIs.keys().forEach(filename => {
    /* ------- API network configuration ---------- */
    const apiDescription = availableAPIs(filename)
    /*  If filename is "Sample.service.js", service name will be "Sample"
        and it will be accessible at App level by `this.$Frameworks`          */
    const serviceName = filename.split('/').pop().split('.')[0]
    // We grab the base URL for this API
    const url = apiDescription.default.baseURL
    // Interceptors
    const interceptors = apiDescription.default.interceptors
    // Base URL reconstitution
    const baseURL = [
      url.protocol,
      '://',
      url.host,
      (['', undefined, '80', 80].includes(url.port)) ? '' : ':' + url.port,
      url.root
    ].join('')

    /* ----- Axios instance configuration ----- */
    const config = { ...apiDescription.default.config, baseURL: baseURL }
    const _axios = axios.create(config)
    _axios.interceptors.request.use(interceptors.request.configInterceptor, interceptors.request.errorInterceptor)
    _axios.interceptors.response.use(interceptors.response.responseInterceptor, interceptors.response.errorInterceptor)

    /* ----- API modelization ------------ */
    // "routesMethods"
    const routesURLs = apiDescription.default.routesURLs

    const methods = {}
    Object.keys(routesURLs).forEach(k => {
      const routesMethods = apiDescription.default.routesMethods(_axios, k, routesURLs[k])
      methods[k] = routesMethods
    })
    console.debug(methods)

    // Eventually we inject the service at Vue prototype level.
    app.config.globalProperties['$' + serviceName] =  methods
  })
}

export default {
  init
}
